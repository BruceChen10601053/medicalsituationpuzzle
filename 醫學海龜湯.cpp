/* 醫學海龜湯程式碼 (UTF-8)
陳光穎 Bruce Chen
2022/9/19
版本: 1.0.0.0
限制: 最多3000題，每題之 題目,答案 最長1000字元
題庫中， '$' 和 '`' 是保留字元。 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/* Global variable */
char WordBank[3000][2][1001]; // 醫學海龜湯題庫
int N; // number of drugs

/* 程式進入點 */
int main(){
    system("chcp 65001"); // 命令提示字元 locale = en_US.UTF-8
    printf("醫學海龜湯\r\n本程式隨機輸出一些症狀或labs/findings，請你猜出病因。按任意鍵看參考答案。答案之大括號代表科別。如欲複製文字，選取後按右鍵即複製，或Ctrl+c。\r\n");
    char c[40] = {-62, -27, -66, -57, -82, -4, -64, 116, -76, -10, -61, 68, -82, 119, 85, 84, 70, 45, 56, 46, 116, 120, 116, 0}; // "醫學海龜湯題庫UTF-8.txt" (ANSI)
    FILE* A = fopen(c, "r");
    if(A == NULL){ // 測試 UTF-8 相容性
        printf("正在嘗試找檔案!\r\n");
        strcpy(c, "醫學海龜湯題庫UTF-8.txt");
        A = fopen(c, "r");
        if(A == NULL){
            printf("找不到檔案。請把 \"醫學海龜湯題庫UTF-8.txt\" 檔名中的中文字去掉，再試一次。 (即：檔名改成 \"UTF-8.txt\"。)\r\n正在嘗試找檔案! (2) \r\n");
            strcpy(c, "UTF-8.txt");
            A = fopen(c, "r");
            if(A == NULL){
                printf("File not found!\n%s\n請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", c);
            }
        }
    }
    int i;
    for(i=0; i<3000; i++){
        char g[2001];
        memset(g, 0, sizeof(g));
        char* f = fgets(g, 1000, A);
        if(f == NULL){
            break;
        }

        int j;
        for(j=0; j<strlen(g); j++){ // 把 ` 換成 \n
            if(g[j] == '`'){
                g[j] = '\n';
            }
        }

        char* s = strtok(g, "$");
        char* t = strtok(NULL, "$");
        if(s == NULL || t == NULL){
            printf("檔案讀取失敗。Syntax error in line %d. 請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", i+1);
            system("pause");
        }
        strcpy(WordBank[i][0], s);
        strcpy(WordBank[i][1], t);
        if(strlen(WordBank[i][0]) > 700 || strlen(WordBank[i][0]) > 700){
            printf("警告: 題庫中有題目或答案過長。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n");
        }
    }
    N = i;
    printf("資料庫有%d個\r\n", N);
    if(N >= 2700){
        printf("警告: 資料庫題數過多。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n");
    }
    srand(time(NULL)); // 設定亂數種子
    system("pause");
    while(1){
        int r = rand() % N; // 產生 0~N 的亂數
        printf("Q: %s. . . ", WordBank[r][0]);
        system("pause");
        printf("A: %s", WordBank[r][1]);
        system("pause");
    }
    return 0;
}

