# 醫學海龜湯

我們來玩個遊戲吧! <br>

## 介紹

程式隨機輸出一些症狀，請你猜出他得了甚麼病。猜完後對參考答案。

## 使用說明

下載 "醫學海龜湯.zip"。解壓縮後，執行 "醫學海龜湯.exe"。<br>
按{Enter}輸出題目。作答完畢請按{Enter}輸出參考答案，再按{Enter}進入下一題。
下載點: https://gitlab.com/BruceChen10601053/medicalsituationpuzzle/-/releases/v1.0.0.1

## 相關作品
[藥理學複習軟體](https://gitlab.com/BruceChen10601053/pharmacologyreview2/-/releases)

[肝達人](https://gitlab.com/BruceChen10601053/EasyLiver/-/releases)

[胃達人](https://gitlab.com/BruceChen10601053/easystomach/-/releases)

[細菌達人](https://gitlab.com/BruceChen10601053/easybacteria/-/releases)

